package upnp

import (
	"io/ioutil"
	// "log"
	"net/http"
	"strconv"
	"strings"
	"log"
	"encoding/xml"
	"fmt"
)

type GetSpecificPortMappingEntry struct {
	upnp *Upnp
	Response GetSpecificPortMappingEntryResponse
}

// Send sends a request to delete a port mapping
// see http://www.javased.com/index.php?source_dir=TomP2P/src/main/java/net/tomp2p/upnp/InternetGatewayDevice.java
func (this *GetSpecificPortMappingEntry) Send(remoteHost string, localPort, remotePort int, protocol string) bool {
	request := this.buildRequest(remotePort, protocol)
	response, err := http.DefaultClient.Do(request)
	if err != nil {
		fmt.Println("GetSpecificPortMappingEntry http req err:", err)
		return false
	}
	resultBody, err := ioutil.ReadAll(response.Body)
	if err != nil {
		fmt.Println("GetSpecificPortMappingEntry failed to read response:", err)
		return false
	}
	if response.StatusCode == 200 {
		// log.Println(string(resultBody))
		this.resolve(string(resultBody))
		if this.Response.NewEnabled == 1 {
			if this.Response.NewInternalClient == this.upnp.LocalHost &&
				this.Response.NewInternalPort == localPort {
					return true
			}
		}
	}
	return false
}

func (this *GetSpecificPortMappingEntry) buildRequest(remotePort int, protocol string) *http.Request {
	// Request header
	header := http.Header{}
	header.Set("Accept", "text/html, image/gif, image/jpeg, *; q=.2, */*; q=.2")
	header.Set("SOAPAction", `"urn:schemas-upnp-org:service:WANIPConnection:1#GetSpecificPortMappingEntry"`)
	header.Set("Content-Type", "text/xml")
	header.Set("Connection", "Close")
	header.Set("Content-Length", "")

	// Request body
	body := Node{Name: "SOAP-ENV:Envelope",
		Attr: map[string]string{"xmlns:SOAP-ENV": `"http://schemas.xmlsoap.org/soap/envelope/"`,
			"SOAP-ENV:encodingStyle": `"http://schemas.xmlsoap.org/soap/encoding/"`}}
	childOne := Node{Name: `SOAP-ENV:Body`}
	childTwo := Node{Name: `m:GetSpecificPortMappingEntry`,
		Attr: map[string]string{"xmlns:m": `"urn:schemas-upnp-org:service:WANIPConnection:1"`}}
	childList1 := Node{Name: "NewExternalPort", Content: strconv.Itoa(remotePort)}
	childList2 := Node{Name: "NewProtocol", Content: protocol}
	childList3 := Node{Name: "NewRemoteHost"}
	childTwo.AddChild(childList1)
	childTwo.AddChild(childList2)
	childTwo.AddChild(childList3)
	childOne.AddChild(childTwo)
	body.AddChild(childOne)
	bodyStr := body.BuildXML()

	// Request
	request, _ := http.NewRequest("POST", "http://"+this.upnp.Gateway.Host+this.upnp.CtrlUrl,
		strings.NewReader(bodyStr))
	request.Header = header
	request.Header.Set("Content-Length", strconv.Itoa(len([]byte(bodyStr))))
	return request
}

// SOAPFault fault
type SOAPFault struct {
	XMLName xml.Name `xml:"http://schemas.xmlsoap.org/soap/envelope Fault"`
	Code    string   `xml:"faultcode,omitempty"`
	String  string   `xml:"faultstring,omitempty"`
	Actor   string   `xml:"faultactor,omitempty"`
	Detail  string   `xml:"detail,omitempty"`
}

type GetSpecificPortMappingEntryResponse struct {
	XMLName    xml.Name `xml:"Envelope"`
	NewInternalPort int `xml:"Body>GetSpecificPortMappingEntryResponse>NewInternalPort"`
	NewInternalClient string `xml:"Body>GetSpecificPortMappingEntryResponse>NewInternalClient"`
	NewEnabled int `xml:"Body>GetSpecificPortMappingEntryResponse>NewEnabled"`
	NewPortMappingDescription string `xml:"Body>GetSpecificPortMappingEntryResponse>NewPortMappingDescription"`
	NewLeaseDuration int `xml:"Body>GetSpecificPortMappingEntryResponse>NewLeaseDuration"`
}

/**
the response for v1 protocol like:
<?xml version="1.0"?>
<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/" s:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
<s:Body>
<u:GetSpecificPortMappingEntryResponse xmlns:u="urn:schemas-upnp-org:service:WANIPConnection:1">
<NewInternalPort>443</NewInternalPort>
<NewInternalClient>192.168.8.201</NewInternalClient>
<NewEnabled>1</NewEnabled>
<NewPortMappingDescription>sys.dashboard.https_port</NewPortMappingDescription>
<NewLeaseDuration>0</NewLeaseDuration>
</u:GetSpecificPortMappingEntryResponse>
</s:Body>
</s:Envelope>
 */
func (this *GetSpecificPortMappingEntry) resolve(resultStr string) {
	//log.Printf("upnp resultStr: \n%s\n", resultStr)
	err := xml.Unmarshal([]byte(resultStr), &this.Response)
	if err != nil {
		log.Printf("xml.Unmarshal error: %v", err)
		return
	}
	//log.Printf("%#v\n", this.Response)
}
